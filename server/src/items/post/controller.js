'use strict'

const PostService = require('./service.js')
const PostRepository = require('./repository.js')


const PostController = {}

// Recuperación de todas las entradas
PostController.getAll = async (req, res, next) => {
    try {


        const post = await PostService.getAll();
        res.status(200).json(post);
    } catch (error) {
        console.log(error)
    } finally {
        next();
    }
};

// Recuperación de una entrada
PostController.getById = async (req, res, next) => {
    try {
        const { id } = req.params
        const post = await PostService.getById(id)
        res.status(200).json(post);
    } catch (error) {
        console.log(error)
    }
    finally {
        next();
    }
};

// Creación de una nueva entrada
PostController.addPost = async (req, res, next) => {
    console.log('esto es el request', req)
    try {

        const body = req.body;
        const userId = req.user[0]._id;
        const nwPst = await PostService.addPost(body, userId)
        res.status(200).json(nwPst);

    } catch (error) {
        console.log(error)
    } finally {
        next();
    }
};

// Modificación de una entrada existente (se actualizará toda la información dela misma, excepto sus comentarios)
PostController.updatePost = async (req, res, next) => {
    try {

        const post = req.body;
        const { id } = req.params;

        const result = await PostService.updatePost(id, post)
        res.status(200).json(result);

    } catch (error) {
        console.log(error)
    } finally {
        next();
        // 
    }
};

// Borrado de una entrada existente 
PostController.deletePost = async (req, res, next) => {
    try {

        const post = req.body;
        const { id } = req.params

        const result = await PostService.deletePost(id, post)
        res.status(200).json({ message: 'this is delete' })

    } catch (error) {
        console.log(error)
    } finally {
        next();
    }
};

PostController.addComment = async (req, res, next) => {
    try {
        const id = req.params.id
        const comment = req.body;
        const postIdNickName = req.user[0]._id
        const query = await PostRepository.getById(id)
        const owneAuthorId = query.postIdAuthor

        const postUpdate = await PostService.addComment(id, comment, postIdNickName, owneAuthorId);
        res.status(200).json(postUpdate)
    } catch (error) {
        console.log(error)
    } finally {
        next();
    }
};

module.exports = PostController

