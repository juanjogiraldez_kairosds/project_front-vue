'use strict'

const CommentSchema = require('./model.js');

const CommentRepository = {};

CommentRepository.getAllComments = async () => {
    return await CommentSchema.find()
}

CommentRepository.getFindOne = async (e) => {
    return await CommentSchema.findOne(e).exec();
}

CommentRepository.addComment = async (idPost, comment, postIdNickName, owneAuthorId) => {
    const newComment = new CommentSchema({nickName: comment.nickName, comment: comment.comment, date: comment.date,  postIdNickName: postIdNickName, authorPostId: idPost, ownerAuthorId: owneAuthorId});
    return await newComment.save();   
}

CommentRepository.updateComment = async (idComment, comment) => {
    return  await CommentSchema.findByIdAndUpdate(idComment, comment, {new: true});
    
}

CommentRepository.deleteComment = async (idComment) => {
    return await CommentSchema.findByIdAndDelete(idComment);
}

module.exports = CommentRepository;