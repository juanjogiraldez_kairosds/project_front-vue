'use strict'

const CommentRepository = require('./repository.js')

const CommentsService = {};

CommentsService.getAllComments = async () => {
    return await CommentRepository.getAllComments();
}

CommentsService.updateComment = async (idComment, comment) => {
    return await CommentRepository.updateComment(idComment, comment);
};

CommentsService.deleteComment = async (idComment) => {
    return await CommentRepository.deleteComment(idComment);
};

module.exports = CommentsService;