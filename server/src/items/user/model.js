'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = Schema({
        user:{ type: String, required: true, index: { unique: true }},
        passwordHash: String,
        role: {type : String, default: 'publisher' }
},{
    timestamps: true
});

module.exports = mongoose.model('user', UserSchema, 'user');