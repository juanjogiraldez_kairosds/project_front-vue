'use strict'

const UserService = require('./service.js')

const UserController = {}

UserController.getFind = async (req, res, next) => {
    
    try{
        const users = await UserService.getFind();
        res.status(200).json(users);
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }finally {
        next();
    };
};

UserController.getFindOne = async (req, res, next) => {
   

    try{
        const users = await UserService.getFindOne(req.params.id);
        res.status(200).json(users);
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }finally {
        next();
    };
};



UserController.addUser = async (req, res, next) => {
    
    const {user,passwordHash,role} = req.body

    try{
        const newUser= await UserService.addUser(user,passwordHash,role);
        res.status(200).json(newUser)
    } catch (err) {
        console.log(err);
        res.status(500).send({message: 'Usuario ya existe'});
    }finally {
        next();
    };


};


module.exports = UserController