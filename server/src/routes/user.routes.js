'use strict'

const UserController = require('../items/user/controller.js');
const UserControl = require('../middlewares/userControl.js');
const passport = require('passport');
const express = require('express');
const user = express.Router();

user.get('/', passport.authenticate('jwt', { session: false }),UserControl.realManager , UserController.getFind);
user.get('/:id',  passport.authenticate('jwt', { session: false }), UserControl.realManager ,UserController.getFindOne);
user.post('/', UserController.addUser);

module.exports = user