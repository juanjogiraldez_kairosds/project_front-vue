'use strict'

const PostRepository = require('../items/post/repository.js')
const CommentRepository = require('../items/comment/repository.js')
const UserControl = {};

UserControl.realManager = async (req, res, next) => {
    try {
        const role = req.user[0].role;

        if (role === 'admin') {
            return next()
        }
        else {
            res.status(401).send({ message: "You are not allowed to do this" });
        }
    } catch (error) {
        console.log(error)
    }

};



UserControl.createEntrie = async (req, res, next) => {

    try {
        const role = req.user[0].role;

        if (role === 'admin' || role === 'publisher') {
            return next()
        }
        else {
            res.status(401).send({ message: "You are not allowed to create entries" });
        }
    } catch (error) {
        console.log(error)
    }

};
// tiene mas relación que este cercano al entorno de "post", en service.js
UserControl.deleteOwnPost = async (req, res, next) => {

    try {
        const role = req.user[0].role;
        const idCurrent = String(req.user[0]._id);
        const { id } = req.params;
        const query = await PostRepository.getById(id);
        const postCreator = String(query.postIdAuthor);

        if (idCurrent === postCreator || role === "admin") {
            return next();
        }
        else {
            res.status(401).send({ message: "You are not allowed to delete" });
        }
    } catch (error) {
        console.log(error)
    }
};

UserControl.updateOnwPost = async (req, res, next) => {

    try {
        const role = req.user[0].role;
        const idCurrent = String(req.user[0]._id);
        const { id } = req.params;
        const query = await PostRepository.getById(id);
        const postCreator = String(query.postIdAuthor);

        if (idCurrent === postCreator || role === "admin") {
            return next();
        }
        else {
            res.status(401).send({ message: "You are not allowed to modify" });
        }
    } catch (error) {
        console.log(error)
    }
}

UserControl.updateOnwComment = async (req, res, next) => {

    try {
        const role = req.user[0].role;
        const idCurrent = String(req.user[0]._id);
        const idComment = req.params.id
        const query = await CommentRepository.getFindOne({ _id: idComment })
        const postCreator = String(query.postIdAuthor);

        if (idCurrent === postCreator || role === "admin") {
            return next();
        }
        else {
            res.status(401).send({ message: "You are not allowed to modify this comment" });
        }
    } catch (error) {
        console.log(error)
    }
}


UserControl.deleteComment = async (req, res, next) => {
    try {
        const role = req.user[0].role
        const idCurrent = String(req.user[0]._id)
        const idComment = req.params.id
        const commentAuthor = await CommentRepository.getFindOne({ _id: idComment })
        const result = commentAuthor.ownerAuthorId;
        if (idCurrent === result || role === "admin") {
            return next();
        } else {
            res.status(401).send({ message: "you are not allowed to delete" })
        }

    }
    catch (error) {
        console.log(error)
    }
}

module.exports = UserControl;
