'use strict'

const offensiveRespository = require('../items/offensiveWord/repository.js');
const CheckOffensiveValidator = require('../validators/check-word.js')
const OffensiveValidator = {}



OffensiveValidator.checkwords = async (req, res, next) => {
    const comment = req.body;
    const offensivewordsDB = await offensiveRespository.getAll();
    
    const offensivewords = offensivewordsDB.map(owdb => {
        return { word: owdb.word, level: owdb.level }
    });


    const offensivewordsFound = CheckOffensiveValidator.check(comment.comment, offensivewords);
    
    if (offensivewordsFound.length === 0) {
        next();
    }else{
        const info = offensivewordsFound.map(ow => ow.word );//+ ' con nivel '
        res.status(403).json({message: 'No puedes utilizar: ' + info});
    }
}

module.exports = OffensiveValidator ;
