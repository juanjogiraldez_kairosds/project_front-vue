'use strict'

const supertest = require ('supertest');
const App = require('../App.js');
const request = supertest(App);



describe('API REST system tests', () => {

    it('Test add a post', async () => {

            // llamada al Login( enmendar)
            const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJib2R5Ijp7Il9pZCI6IjVlYTgzYWU5MzYxZGRjMDk0NzY3OTU5NCIsInVzZXJOYW1lIjoidXNlcjEiLCJyb2xlIjoiYWRtaW4ifSwiaWF0IjoxNTg4NTIxNjc3fQ.teNY5jrf_AprMcVW6VyL--XHqOCLmulWguDeSv2TlMg';

            const addPost = await request.post('/api/post/')
                .set("Authorization", 'Bearer ' + token)
                .set('Accept', 'application/json')
                .send({
                    'author': 'Terence David John "Terry" Pratchett',
                    'nickName': 'Terry',
                    'title': 'El color de la Magia', 
                    'text': 'En un lejano juego de dimensiones de segunda mano, en un plano astral ligeramente combado... '
                })
               
                .expect(200)
             
                expect(addPost.body.title).toBe('El color de la Magia')
})

        
    it('Test add a comment to a post entry', async () => { 

        const getAllPost = await request.get('/api/post/')
                .expect(200)
                
        const positionLastPost = getAllPost.body.length - 1
        const { _id }= getAllPost.body[`${positionLastPost}`];
       
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJib2R5Ijp7Il9pZCI6IjVlYTgzYWU5MzYxZGRjMDk0NzY3OTU5NCIsInVzZXJOYW1lIjoidXNlcjEiLCJyb2xlIjoiYWRtaW4ifSwiaWF0IjoxNTg4NTIxNjc3fQ.teNY5jrf_AprMcVW6VyL--XHqOCLmulWguDeSv2TlMg';

        const addComment = await request.put(`/api/post/${_id}/comment`)
            .set("Authorization", 'Bearer ' + token)
            .set('Accept', 'application/json')
            .send({
                'nickName': 'Terry(user1)',
                'comment' : 'Esto una prueba de un comentario'
            })

            .expect(200)

            const getAllComment = await request.get('/api/comment/')
            .expect(200)

            const positionLastComment = getAllComment.body.length - 1
            const lastCommentBody= getAllComment.body[`${positionLastComment}`];

            expect(lastCommentBody.nickName).toBe('Terry(user1)')
            expect(lastCommentBody.comment).toBe('Esto una prueba de un comentario')
            
    })

    it('test delete a comment from a post entry', async () => {

        const getAllComment = await request.get('/api/comment/')
        expect(200)
        

        const positionLastComment = getAllComment.body.length - 1
        const { _id }= getAllComment.body[`${positionLastComment}`];
        console.log(getAllComment.body[`${positionLastComment}`])
        console.log(_id)

        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJib2R5Ijp7Il9pZCI6IjVlYTgzYWU5MzYxZGRjMDk0NzY3OTU5NCIsInVzZXJOYW1lIjoidXNlcjEiLCJyb2xlIjoiYWRtaW4ifSwiaWF0IjoxNTg4NTIxNjc3fQ.teNY5jrf_AprMcVW6VyL--XHqOCLmulWguDeSv2TlMg';
        
        const deleteComment = await request.delete(`/api/comment/${_id}`)
            .set("Authorization", 'Bearer ' + token)
            .set('Accept', 'application/json')

        expect(200)

        const getLastComment = await request.get('/api/comment/')
        expect(200)

        const LastComment = getLastComment.body.length - 1
        const ultimeComment= getAllComment.body[`${LastComment}`];
        console.log(ultimeComment._id)

        expect(_id).not.toBe(ultimeComment._id)

    })

});

module.exports = {
    testEnvironment: 'node'
  };