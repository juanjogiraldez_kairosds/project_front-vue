import { applyPolyfills, defineCustomElements } from 'blog-angular/loader';
import Vue from 'vue';
import App from './App.vue';
import router from './router/index.route';
import store from './store/index';

Vue.config.productionTip = false
Vue.config.ignoredElements = [/my-\w*/];

applyPolyfills().then(() => {
  defineCustomElements(window);
});



new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
