import axios from 'axios'
import '../shared/interceptor'

export default new class PostService {

    constructor() { }

    async allPosts() {
        try {
            const getPost = await axios.get('http://localhost:3000/api/post')
            return getPost.data
        }
        catch (error) {
            console.log(error)
        }
    }

    async onePost(id) {
        try {
            const getAPost = await axios.get(`http://localhost:3000/api/post/${id}`)
            return getAPost.data
        }
        catch (error) {
            console.log(error)
        }
    }

    async newPost(body) {
        try {
            console.log( 'body',  body)
            return await axios.post('http://localhost:3000/api/post', body)
        } catch (error) {
            console.log(error)
        }
    }

    async modifyPost(id, body) {
        try {
            return await axios.put(`http://localhost:3000/api/post/${id}`, body)
        } catch (error) {
            console.log(error)
        }
    }

    async deletePost(id) {
        try {
            return await axios.delete(`http://localhost:3000/api/post/${id}`)
        } catch (error) {
            console.log(error)
        }
    }

    async newComment(id, body) {
        try {
            return await axios.put(`http://localhost:3000/api/post/${id}/comment`, body)
        } catch (error) {
            console.log(error)
        }
    }

    async modifyComment(id, body) {
        try {
            return await axios.put(`http://localhost:3000/api/comment/${id}`, body)
        } catch (error) {
            console.log(error)
        }
    }

    async deleteComment(id) {
        try {
            return await axios.delete(`http://localhost:3000/api/comment/${id}`)
        } catch (error) {
            console.log(error)
        }
    }


}


