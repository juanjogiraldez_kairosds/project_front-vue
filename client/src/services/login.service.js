import axios from 'axios';

export default new class LoginService {

    constructor() { }

    async login(e) {
        try {
            const result = axios.post('http://localhost:3000/api/login/', {}, {
                auth: { username: e.username,password: e.password}
            }).then(res => {
                 localStorage.setItem('token', res.data.token)
            })
            return result
        }
        catch (error) {
            console.log(error)
        }
    }

    async signUp (data) {

        try {
            return  await axios.post('http://localhost:3000/api/user', data)
        }
        catch(error) {
            console.log(error)
            }
        }
    }