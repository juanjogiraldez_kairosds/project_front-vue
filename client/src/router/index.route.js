import Vue from 'vue';
import Router from 'vue-router';
import Allpost from '../components/home/allPost';
import Login from '../components/login/login';
import Accessmain from '../views/accessmain';
import BackOffice from '../views/backoffice';
import Homemain from '../views/homemain';



Vue.use(Router);


export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', redirect: '/Homemain', name: 'homemain' },
    {
      path: '/Homemain',  component: Homemain, children: [
        { path: '', name: 'allPost', component: Allpost },
        {
          path: ':id', name: 'postDetail',
          component: () => import(/*webpackChunkName: postDetail */ '../components/home/postDetail')
        }
      ]
    },
    {
      path: '/Accessmain',  component: Accessmain, children: [
        { path: '', name: 'login', component: Login },
        {
          path: 'Registro', name: 'signUp',
          component: () => import(/* webpackChunkName: registro */ '../components/login/signUp')
        }]
    },
    {
      path: '/Backoffice',  component: BackOffice, beforeEnter: isLogin, children: [
        {
          path: '', name: "backOfficePost",
          component: () => import(/* webpackChunkName: registro */ '../components/backOffice/Posts/BackAllPost'),
        },
        {
          path: 'CreatePost', name: "backCreatPost",
          component: () => import(/* webpackChunkName: registro */ '../components/backOffice/Posts/BackCreatPost')
        },
        {
          path: ':id', name: "backModifyPost",
          component: () => import(/* webpackChunkName: registro */ '../components/backOffice/Posts/BackModifyPost')
        },
        {
          path: 'PostDetail/:id', name: "backOfficePostDetail",
          component: () => import(/* webpackChunkName: registro */ '../components/backOffice/PostDetail/BackPostDetail')
        },
        {
          path: 'PostDetail/:id/CreateComment', name :'B-PostDetailCreateComment', 

          component: () => import(/*  webpackChunkName: createComment */ '../components/backOffice/PostDetail/BackCCPD')
        },
        {
          path: 'PostDetail/:id/ModifyComment/:id2', name :'B-PostDetailModifyComment', 

          component: () => import(/*  webpackChunkName: ModifyComment */ '../components/backOffice/PostDetail/BackMCPD')
        }
      ]
    }
  ]
})

function isLogin(to, from, next) {

  const token = localStorage.getItem('token');
  if (!token) {
    next('/Accessmain')
  } else {
    next()
  }
}