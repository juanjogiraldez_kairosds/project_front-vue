
import Vue from 'vue';
import Vuex from 'vuex';
import Service from '../services/post.service';

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        posts: [],
        post: {},
        // views: false
    },
    getters: {
        posts: state => { return state.posts },
        post: state => { return state.post },
        // views: state => { return state.views },
    },
    actions: {


        allPosts({ commit }) {
            Service.allPosts()
                .then(res => commit('allPosts', res))
                .catch(error => console.log(error))
        },

        onePost({ commit }, id) {
            Service.onePost(id)
                .then(res => {commit('onePost', res)} )
                .catch(error => console.log(error))
        },

        newPost({ commit }, post) {
            Service.newPost(post)
                .then(res => { commit('newPost', res.data) })
                .catch(error => console.log(error))
        },

        modifyPost({ commit }, post) {
            
            Service.modifyPost(post.id, post.post)
                .then(res => { commit('modifyPost', res.data) })

        },

        deletePost({ commit }, id) {
            Service.deletePost(id)
                .then(() => commit('deletePost', id))
                .catch(error => console.log(error))
        },

        newComment({ commit }, comment) {
            Service.newComment(comment.id, comment.comment)
                .then(() => { commit('newComment', comment) })
                .catch(error => console.log(error))
        },

        modifyComment({ commit }, comment) {
            Service.modifyComment(comment.id, comment.comment)
                .then(res => { commit('modifyComment', res.data) })
                .catch(error => console.log(error))
        },

        deleteComment({ commit }, id) {
            Service.deleteComment(id)
                .then(() => commit('deleteComment', id))
                .catch(error => console.log(error))
        },

        modifyViews({commit}) {
            () => commit('modifyViews')
        }

    },

    mutations: {

        allPosts(state, posts) { state.posts = posts; },

        onePost(state, post) { state.post =  post; },

        newPost(state, post) { state.posts = [...state.posts, post]; },

        modifyPost(state, post) {
            const objPost = Object.assign({}, post);
            const posts = state.posts;
            const index = posts.findIndex(e => e._id === objPost._id)
            state.posts = [...posts.slice(0, index), objPost, ...posts.slice(index + 1)];
        },

        deletePost(state, id) { state.posts = state.posts.filter(post => post._id !== id); },

        newComment(state, comment) {
            state.post.comments= [...state.post.comments, comment.comment]
            // const newComment = [...state.post.comments, comment];
            // state.post = { ...state.post, comments: newComment };
        },
        modifyComment(state, comment) {
            const objComment = Object.assign({}, comment);
            const comments = state.post.comments;
            const index = comments.findIndex(e => e._id === objComment._id);
            const newComment = [...state.post.comments.slice(0, index), objComment, ...state.post.comments.slice(index + 1)];
            state.post = { ...state.post, comments: newComment };
        },

        deleteComment(state, id) {
            const comments = state.post.comments.filter(comment => comment._id !== id);
            state.post = { ...state.post, comments: comments }
        }
        ,
        // modifyViews(state){
        //     if (state.views == true){
        //         state.views == false
        //     } else {
        //         state.views == true
        //     }
        // }


    }
});